package com.senghuy.intentobjects;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private final static int REQUEST_CODE_1 = 1;

    //declare variable and objects
    User user = new User();
    Button button;
    EditText txtFullName, txtEmail, txtPassword, txtConfirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get value from edittext
        txtFullName = findViewById(R.id.fullname);
        txtEmail = findViewById(R.id.email);
        txtPassword = findViewById(R.id.password);
        txtConfirmPassword = findViewById(R.id.confirm_password);

        //event pass data to Detail Screen
        button = findViewById(R.id.btnSend);
        button.setOnClickListener(view -> {

            //get value from edittext into object
            user.setFullName(txtFullName.getText().toString());
            user.setEmail(txtEmail.getText().toString());
            user.setPassword(txtPassword.getText().toString());

            //validate email
            String email = txtEmail.getText().toString().trim();
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

            //Validate and pass value into DetailActivity
            if (user.fullName.equals("")) {
                Toast.makeText(this, "Please enter your name! ", Toast.LENGTH_SHORT).show();
            }else if (user.email.equals("")){
                Toast.makeText(this, "Please enter your email! ", Toast.LENGTH_SHORT).show();
            }else if (!email.matches(emailPattern)){
                Toast.makeText(this, "Invalid Email! ", Toast.LENGTH_SHORT).show();
            }else if (user.password.equals("")){
                Toast.makeText(this, "Please enter your password! ", Toast.LENGTH_SHORT).show();
            }else if (!txtPassword.getText().toString().equals(txtConfirmPassword.getText().toString())){
                Toast.makeText(this, "Confirm password is not correct! ", Toast.LENGTH_SHORT).show();
            }else {
                //pass to DetailActivity
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra("FullName", user.fullName);
                intent.putExtra("Email", user.email);
                intent.putExtra("Password", user.password);
                startActivityForResult(intent,REQUEST_CODE_1);
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode)
        {
            // This request code is set by startActivityForResult(intent, REQUEST_CODE_1) method.
            case REQUEST_CODE_1:
                if(resultCode == RESULT_OK)
                {
                    if (txtFullName != null && txtEmail != null && txtPassword != null && txtConfirmPassword != null) {
                        txtFullName.setText("");
                        txtEmail.setText("");
                        txtPassword.setText("");
                        txtConfirmPassword.setText("");
                    }

                    String messageReturn = data.getStringExtra("message_return");
                    Context context = getApplicationContext();
                    CharSequence text = messageReturn;
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
        }

    }
}
