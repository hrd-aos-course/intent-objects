package com.senghuy.intentobjects;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    TextView txtFullName, txtEmail, txtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //getData from Main
        txtFullName = findViewById(R.id.p_fullname);
        txtEmail = findViewById(R.id.p_email);
        txtPassword = findViewById(R.id.p_password);

        Intent intent = getIntent();
        String fullName = intent.getStringExtra("FullName");
        String email = intent.getStringExtra("Email");
        String password = intent.getStringExtra("Password");

        txtFullName.setText(fullName);
        txtEmail.setText(email);
        txtPassword.setText(password);
        //getData from Main end

        // Click this button to send response result data to MainActivity.
        Button passDataTargetReturnDataButton = findViewById(R.id.p_btnBack);
        passDataTargetReturnDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("message_return", "Please create a new User");
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

}
